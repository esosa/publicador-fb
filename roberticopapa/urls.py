"""roberticopapa URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
import publicador.views

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^$', publicador.views.home_page, name='home'),
    url(r'^publicador/$', publicador.views.home_page, name='home'),

    url(r'^publicador/crea_publicacion$', publicador.views.crea_publicacion, name='crea_publicacion'),
    url(r'^publicador/(\d+)/recibe_fotos$', publicador.views.recibe_fotos, name='recibe_fotos'),
    url(r'^publicador/(\d+)/guarda_publicacion$', publicador.views.guarda_publicacion, name='guarda_publicacion'),
    url(r'^publicador/(\d+)/ver_publicacion$', publicador.views.ver_publicacion, name='ver_publicacion'),
    url(r'^publicador/imagenes/(.+)$', publicador.views.ver_imagen, name='ver_imagen'),
    url(r'^publicador/(\d+)/publicar$', publicador.views.publicar, name='publicar'),
    url(r'^publicador/(\d+)/eop$', publicador.views.eop, name='eop'),
    url(r'^publicador/(\d+)/editar$', publicador.views.editar, name='editar'),
    url(r'^publicador/(\d+)/eliminar$', publicador.views.eliminar, name='eliminar'),
    url(r'^publicador/cambia_contrasena$', publicador.views.cambia_contrasena, name='cambia_contrasena'),

]
