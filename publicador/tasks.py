from __future__ import absolute_import
from celery import shared_task

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

from time import sleep
from re import search

import unittest

from publicador.models import Publicacion, PublicacionImagen, PublicacionFecha, Cuenta

@shared_task
def enviar(publicacion_id):
    cuenta = Cuenta.objects.all()[0]
    semail = cuenta.email
    spw = cuenta.password

    try:
        publicacion = Publicacion.objects.get(id=publicacion_id)
    except Publicacion.DoesNotExist:
        print("el objeto ha sido eliminado")
    else:
        driver = webdriver.Firefox()
        driver.get('https://www.facebook.com/groups/%s/' % publicacion.grupo)
        driver.implicitly_wait(15)
        wait = WebDriverWait(driver, 600)
        print("nos metimos")

        email = driver.find_element_by_id('email')
        pw = driver.find_element_by_id('pass')

        email.send_keys(semail)
        pw.send_keys(spw)

        try:
            enviar = driver.find_element_by_xpath("//label[@id='loginbutton']/input[1]")
        except:
            enviar = driver.find_element_by_xpath("//button[@id='loginbutton']")

        enviar.send_keys(Keys.ENTER)
        print("mandamos credenciales")

        driver.find_element_by_xpath("//div[@data-testid='react-composer-root']/div[1]/div[1]/div[1]/div[1]/ul[1]/li[1]/a[1]").send_keys(Keys.ENTER)

        cuadro = driver.find_element_by_xpath("//div[@data-testid='react-composer-root']/div[2]/div[1]/div[1]/div[1]/div[2]/label[1]/input[1]")
        cuadro.send_keys(publicacion.nombre)
        print("nombre")

        cuadro = driver.find_element_by_xpath("//div[@data-testid='react-composer-root']/div[2]/div[1]/div[1]/div[2]/div[4]/label[1]/input[1]")
        cuadro.send_keys(publicacion.precio)
        print("precio")

        cuadro = driver.find_element_by_xpath("//div[@data-testid='react-composer-root']/div[2]/div[1]/div[1]/div[4]/div[1]/div[1]")
        cuadro.send_keys(publicacion.descripcion)
        print("descripcion")

        imagenes = [pi.imagen for pi in PublicacionImagen.objects.filter(publicacion_id=publicacion.id)]
        for path in [imagen.path for imagen in imagenes]:
            cuadro = driver.find_element_by_xpath("//input[@data-testid='add-more-photos']").send_keys(path)
            print("imagen")
            sleep(5)

        print("esperamos al boton")

        enviar = wait.until(EC.element_to_be_clickable((By.XPATH, "//button[@data-testid='react-composer-post-button']")))
        enviar.send_keys(Keys.ENTER)

        sleep(10)
        driver.quit()
        print("listo")

if __name__ == '__main__':
    pass
