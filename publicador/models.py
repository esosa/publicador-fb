from django.db import models
import datetime

class Publicacion(models.Model):
    grupo = models.TextField(default='')
    nombre = models.TextField(default='')
    precio = models.TextField(default='')
    descripcion = models.TextField(default='')
    numero_fotos = models.IntegerField(default=0)
    numero_publicaciones = models.IntegerField(default=1)
    def __str__(self):
        return self.nombre

class PublicacionImagen(models.Model):
    publicacion = models.ForeignKey(Publicacion, default=None)
    md5 = models.TextField(default='')
    imagen = models.ImageField(upload_to='publicador/imagenes', default=None)

class PublicacionFecha(models.Model):
    publicacion = models.ForeignKey(Publicacion, default=None)
    celery_id = models.TextField(default='')
    fecha = models.DateTimeField(default=datetime.datetime.now)
    cancelado = models.BooleanField(default=False)

class Cuenta(models.Model):
    email = models.TextField(default='')
    password = models.TextField(default='')
