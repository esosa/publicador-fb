from django.shortcuts import render, redirect
from django.core.files.base import ContentFile
from publicador.models import Publicacion, PublicacionImagen, PublicacionFecha, Cuenta
from django.http import HttpResponse
import mimetypes
from publicador.tasks import enviar
import datetime
import hashlib
from pytz import timezone
from celery.task.control import revoke

# Create your views here.
def home_page(request):
    if len(Cuenta.objects.all()) == 0:
        return render(request, 'cambia_contrasena.html')
    else:
        publicaciones = Publicacion.objects.all()
        enviados = []
        for publicacion in publicaciones:
            if len(PublicacionFecha.objects.filter(publicacion_id=publicacion.id)) > 0 and PublicacionFecha.objects.filter(publicacion_id=publicacion.id)[0].celery_id != '':
                enviados.append(True)
            else:
                enviados.append(False)
        return render(request, 'home.html', {
            'pe': zip(publicaciones, enviados)
        })

def crea_publicacion(request):
    publicacion = Publicacion.objects.create(
        grupo = request.POST.get('grupo_texto', ''),
        nombre = request.POST.get('nombre_texto', ''),
        precio = request.POST.get('precio_texto', ''),
        descripcion = request.POST.get('descripcion_texto', ''),
        numero_fotos = (0 if request.POST.get('num_fotos') == '' else int(request.POST.get('num_fotos', 0))),
        numero_publicaciones = (0 if request.POST.get('num_publicaciones') == '' else int(request.POST.get('num_publicaciones', 0))),
    )

    return redirect('/publicador/%d/recibe_fotos' % (publicacion.id,))

def cambia_contrasena(request):
    email = request.POST.get('email_texto', default='')
    password = request.POST.get('password_texto', default='')
    if email == '':
        return render(request, 'cambia_contrasena.html', {
            'cuenta': Cuenta.objects.all()[0] if len(Cuenta.objects.all()) != 0 else None,
            'cambio': False,
        })
    else:
        if len(Cuenta.objects.all()) == 0:
            Cuenta.objects.create(email = email, password = password)
        else:
            cuenta = Cuenta.objects.all()[0]
            cuenta.email = email
            cuenta.password = password
            cuenta.save()
        return render(request, 'cambia_contrasena.html', {
            'cuenta': Cuenta.objects.all()[0] if len(Cuenta.objects.all()) != 0 else None,
            'cambio': True,
        })

def recibe_fotos(request, publicacion_id):
    publicacion = Publicacion.objects.get(id = publicacion_id)
    return render(request, 'recibe_fotos.html', {
        'range_imagenes': range(publicacion.numero_fotos),
        'range_fechas': range(publicacion.numero_publicaciones),
        'publicacion': publicacion,
    })

def eliminar(request, publicacion_id):
    for tarea in PublicacionFecha.objects.filter(publicacion_id=publicacion_id):
        revoke(tarea.celery_id)
        tarea.delete()
    for imagen in PublicacionImagen.objects.filter(publicacion_id=publicacion_id):
        imagen.delete()
    Publicacion.objects.get(id = publicacion_id).delete()
    return redirect('/publicador/')
    

def guarda_publicacion(request, publicacion_id):
    publicacion = Publicacion.objects.get(id = publicacion_id)

    publicacion.grupo = request.POST.get('grupo_texto', '')
    publicacion.nombre = request.POST.get('nombre_texto', '')
    publicacion.precio = request.POST.get('precio_texto', '')
    publicacion.descripcion = request.POST.get('descripcion_texto', '')
    publicacion.save()

    for archivo in request.FILES.getlist('imagen'):
        barchivo = archivo.read()
        if len(PublicacionImagen.objects.filter(publicacion=publicacion, md5 = hashlib.md5(barchivo).hexdigest())) == 0:
            pi = PublicacionImagen.objects.create(publicacion=publicacion)
            pi.md5 = hashlib.md5(barchivo).hexdigest()
            pi.imagen.save('%s_%s' % (publicacion_id, archivo.name), archivo)
            pi.save()
    for fecha in request.POST.getlist('fecha'):
        if len(PublicacionFecha.objects.filter(fecha = timezone('America/Caracas').localize(datetime.datetime.strptime(fecha, '%Y-%m-%dT%H:%M')))) == 0:
            pi = PublicacionFecha.objects.create(publicacion=publicacion, fecha = timezone('America/Caracas').localize(datetime.datetime.strptime(fecha, '%Y-%m-%dT%H:%M')))
    return redirect('/publicador/%d/ver_publicacion' % (publicacion.id,))

def ver_imagen(request, nombre):
    f = open("publicador/imagenes/%s" % nombre, mode="rb")
    imagen = f.read()
    response = HttpResponse(imagen, content_type=mimetypes.guess_type(nombre))
    return response

def eop(request, publicacion_id):
    editar = request.POST.get('editar', False)
    publicar = request.POST.get('publicar', False)
    eliminar = request.POST.get('eliminar', False)
    if editar and not publicar and not eliminar:
        return redirect('/publicador/%s/editar' % (publicacion_id,))
    elif publicar and not eliminar and not editar:
        return redirect('/publicador/%s/publicar' % (publicacion_id,))
    elif eliminar and not publicar and not editar:
        return redirect('/publicador/%s/eliminar' % (publicacion_id,))
    return None

def editar(request, publicacion_id):
    publicacion = Publicacion.objects.get(id = publicacion_id)
    return render(request, 'editar.html', {
        'publicacion': publicacion,
    })
    pass

def ver_publicacion(request, publicacion_id):
    publicacion = Publicacion.objects.get(id = publicacion_id)
    imagenes = [x.imagen for x in PublicacionImagen.objects.filter(publicacion_id=publicacion_id)]
    fechas = [publicacionfecha.fecha.astimezone(timezone("America/Caracas")).strftime('%Y-%m-%d %I:%M %p') for publicacionfecha in PublicacionFecha.objects.filter(publicacion_id=publicacion_id)]
    if len(PublicacionFecha.objects.filter(publicacion_id=publicacion_id)) > 0: # este condicional es una basura y ofensivo
        return render(request, 'ver_publicacion.html', {
            'publicacion': publicacion,
            'imagenes': imagenes,
            'fechas': fechas,
            'enviado': True if PublicacionFecha.objects.filter(publicacion_id=publicacion_id)[0].celery_id != '' else False
        })
    else:
        return render(request, 'ver_publicacion.html', {
            'publicacion': publicacion,
            'imagenes': imagenes,
            'fechas': fechas,
            'enviado': False
        })

def publicar(request, publicacion_id):
    publicacion = Publicacion.objects.get(id = publicacion_id)
    imagenes = [pi.imagen for pi in PublicacionImagen.objects.filter(publicacion_id=publicacion_id)]
    rutas_imagenes = [imagen.path for imagen in imagenes]
    for pf in PublicacionFecha.objects.filter(publicacion_id=publicacion_id):
        if pf.celery_id == '':
            pf.celery_id = enviar.apply_async([publicacion.id], eta=pf.fecha,).id
            pf.save()
            h = 'Se envió la publicación'
        else:
            h = 'La publicación ya había sido enviada'
    return render(request, 'publicar.html', {
        'publicacion': publicacion,
        'imagenes': imagenes,
        'header': h
    })
